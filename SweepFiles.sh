#!/bin/bash
##
# Delete all files over N hours old from a directory.
# Add this to launchd to run hourly.



# Directory to monitor
MONITOR='/Users/outofcontrol/Desktop/Outbox'

# Hours to keep files for
KEEP=3000

######################
# END OF CONFIGURATION
######################

if [[ $# -eq 1 ]]; then
    base=$1
else
    base="."
fi

saveIFS=$IFS
IFS=$'\n'

TR=$(which TR)
YESTERDAY=`date -v-${KEEP}H +%s`
kMDItemFSName=""

FILES=$(mdls -name kMDItemDateAdded -name kMDItemFSName $MONITOR/*)
for i in $FILES;
do
     eval $(sed "s/[[:blank:]]*=[[:blank:]]*\(.*\)$/='\1'/g" <<< $i;)
     TS=$(date -j -f "%Y-%m-%d %T +0000 %Z" "$kMDItemDateAdded EST" "+%s")
     if [ "$kMDItemFSName" != "" ];
     then 
        if [ $TS -lt $YESTERDAY ];
        then 
            FILETODELETE=$($TR -d '"' <<< $MONITOR/$kMDItemFSName)
            osascript -e "tell application \"Finder\" to delete POSIX file \"$FILETODELETE\"" 2>&1 > /dev/null
        fi
        kMDItemFSName="";
     fi
done

IFS=$saveIFS

exit 0
