## File Sweeper

OS: macOS

A simple bash script to automate moving to Trash files that are over a certain age. 

The script looks at the metadata using mdls to see at what date and time each file was added to a specificed folder. By running this script hourly in a cron or launchd, files that have been added to the designated folder will be moved to the Trash. 

This script does not remove files based on creation date or last modified date. 

Improvements are welcome. 